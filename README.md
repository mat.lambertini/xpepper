# Phoenix Application Solution


## Requirements

* Vagrant, version 2.x
    * https://www.vagrantup.com/docs/installation/
* Virtualbox
    * https://www.virtualbox.org/wiki/Downloads
* Ansible, version 2.5.x or higher
    * https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html


## Setup Infrastructure

By default it's going to build a 3 node swarm cluster. One manager and two worker, those numbers can be changed in the 'infra/global-config.yml'.
In order to have a cluster that's resilient to faults and crashes you need to have redundancy on the masters. An odd number(>=3) of masters is needed and suggested. The actual amount depends on the number of faults you want to tolerate.

    cd infra
    vagrant up

Many aspects of the cluster can be configured by variables in the 'infra/global-config.yml'.

## Deploy the webapp

In order to be able to deploy the webapp you need to create a file 'vpassword' with the key to decrypt the vault file secrets.yml.
The key is: KbpYugx689YGFB88UDfU

    echo "KbpYugx689YGFB88UDfU" > infra/vpassword

Once you've created that file you can deploy the app with the following commands:

    cd infra
    ansible-playbook deploy.yml -e "status=up" --limit node-1

Depending on your network bandwith it can take several minutes to download the docker images and start all the containers.
Before the application can be reacheable the mongodb replica set needs to be built and in the meantime the application will try to connect to the mongodb cluster repeatidly.

By default you can access the application at 'http://localhost:8080'.

Removing the application from the cluster can be done with:

    cd infra
    ansible-playbook deploy.yml -e "status=down" --limit node-1


The inventory file's path and the vault file's path are configured in the 'infra/ansible.cfg' file. You should not need to change it.


## Clean up

Once the test is done you can delete everything by doing:

    cd infra
    vagrant destroy


# Design aspects and known Limitations

The services on top of the docker swarm cluster are:

Clients <-(DL)-> Layer of nginx reverse proxies <-(DL)-> layer of phoenix-kata webapps <--> mongodb replica set of 3 nodes.

DL = Docker load balancer

Recent versions of Swarm provide a load balancer funcionality between services via a virtual ip for each service. This virtual ip can be retrieved by resolving (dns) the service name in any container in the stack. This functionality makes redundant any external tool for service discovery in some use cases.
More info on [Docker Load Balancing](https://docs.docker.com/engine/swarm/key-concepts/#load-balancing)

## CI/CD - No Continuos Deployment

The CI/CD has been implemented on gitlab ci/cd. It executes the application tests, creates a docker image and publish the new image in the gitlab registry.
There's no continuos delivery since the cluster is running in vagrant+virtualbox on a local machine that's not easily reachable from the internet.

## Monitoring

In a real world scenario you usually have a centralized monitoring tool (like prometeus, nagios,..) to do white box monitoring that cover all your infrastructure. You also usually have an external (somewhere on the internet) monitoring platform (like uptimerobot.com) to do black box monitoring. In this POC I've decided to monitor the docker hosts and the containers using [netdata](https://my-netdata.io/). This gives me the reliability of a distributed monitoring system without the need for a central server. Though if you lose one docker host you also lose access to the monitoring data of that particular host. It's not implemented in my configuration but it's possible to add a backend for archiving the metrics.

The URL to see the node-1 monitoring interface is http://localhost:19999. To actually see the monitoring of the other nodes you need to figure out which ports has been automatically forward by vagrant (it was printed during the vagrant up phase) or using this command (on linux):

    netstat -ltnp |grep -i -E "0.0.0.0:2.*vbox" |awk  '{ print $4 }' |cut -d: -f2

Once you've visited the URL once netdata is going to remember all the urls and show it in the menu on the top left corner.

### Monitoring notification

I've decided to use slack to receive the notifications. This add an extra step of getting an incoming webhook from slack. I didn't find a way to automate this part.
To test it you can change the slack webhook in the 'infra/global-config.yml' file and run:

    cd infra
    vagrant up --provision

To obtain a webhook from slack: https://api.slack.com/incoming-webhooks

## No DNS and HTTPS

Although obtaining a DV ssl certificate could have been easily automated with letsencrypt, the lack of a static public ip prevented me from obtaining a dns name that could have been eligible for it. With a public static ip I would have automated the dns registration (using cloudfare for example) and the DV ssl certificate generation and renewal.

## Missing a shared storage

The current implementation lack a shared storage to store persistent data and make it available to all nodes. This made me fall back to local storage and placement constraints on specific nodes. This approach may creates single point of failures.

## No external log server

The infrastructure should have a central log system that's both highly available and distributed. The current implementation centralize the logs on manager-1, this makes manager-1 a single point of failure for the log system. In order to limit the damage it's possible to set the log driver to 'journald' to make it store the logs both on each docker hosts and in the central syslog server.

## No external backup solution

In a real world scenario you usually have in place an external backup solution that manage the backups for your whole cluster. In this POC I've taken a shortcut and use manager-1 as the backup server. This would not be in any way a wise decision since the backups should be kept in a separate reliable system.

## No Autoscaling

Currently docker Swarm doesn't support autoscaling by default. There're some external tools that can be used such as [Orbiter](https://github.com/gianarb/orbiter). This tool offer a REST interface to scale up or down a service. It does not offer monitoring/rule expression part need for autoscaling to be actually used. It would have been fairly easy to write a script and integrate the check on netdata or other monitoring systems. What prevented me from implementing it was that:

* the reverse proxy may be composed by many containers behind the docker load balancer. The number of nginx containers can scale up or down. Even though I'd configured the stub_status module on nginx that expose the request/s it's actually only meaningful when there's just one reverse proxy. A more sophisticated monitoring script can be written to take into account all reverse proxies and all web applications but I thought it was to much for a POC;
* the node.js didn't expose the request/s information;
* lack of experience on my side with node.js platform.

Manual scaling can still be achieved by running:

    cd infra
    vagrant ssh node-1
    docker service scale xpeppers_phoenix-kata=<number of instances>
